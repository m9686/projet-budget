# Projet Gestion de Budget

## L'interface graphique :

L'interface graphique, réalisée en Tailwind, est composée : 

- D'un bloc formulaire recevant les entées de l'utilisateur.
- D'une seconde partie contenant les informations relatives au compte, dont cerraines ont vocation à évoluer en fonction des information saisies pas l'utilisateur.
- Enfin d'un troisième bloc qui montre les opérations éffectuées.
  

## Maquette Desktop :

![](./maquette/MaquetteOrdi.png)

## Maquette Portable :

![](./maquette/MaquettePortable.png)

## La Structure du Projet :

Le projet contient : 

- Un fichier index.html contient toute l'interface de l'application.
- Un fichier entities.ts qui contient les deux classes(Account et Operation).
- Un fichier main.ts(un peu trop chargé...) qui contient tout les script de l'application.
- Tous les autres fichier sont ceux qui ont été automatiquement généré par Vite.


## Fonctionnement de l'application :

Les premières ligne du fichier main.js servent surtout à récupérer les différents éléments du DOM et à déclarer les variables et les listes qui auront leur utlité dans plusieurs fonctions.

La première fonction, non pas par sa position dans le fichier, mais par l'odre logique de l'application, est la fonction takeInputData(). Son rôle est, comme son nom l'indique, de récupérer les données entrées par l'utilisateur dans le formulaire, et, à partir de ceux-ci, d'instancier un Objet de la classe Operation. 

Vient ensuite la fonction createOp() qui créer l'affichage des opérations et utilise les valeurs d'un objet de la classe Operation pour générer leur contenu.

Ces deux fonctions sont à la fois liées et activées pas un addEventListener() attaché au bouton du formulaire.
Il recupère l'objet instancié par takeInputData(), le place dans une liste d'objets Operation. Il utilise ensuite la fonction createOp dans un boucle foreach afin de générer un composant HTML pour chaque Operation présente dans la liste.

Une fonction deleteOp() supprime une Operation précise dans la lsite, grâce à un bouton supprimer généré à son affichage. 

La partie Account est fixe dans le HTML, mais est représentée par un classe dans le main.ts, afin de pouvoir modifier les valeurs de ses propriétés en fonction des action de l'utilisateur. Par exemple lorsqu'une opération est générée le montant de l'opération est ajouté ou déduite(selon le choix de l'utilisateur) du solde affiché.

Une dernière fonctionalité permet de filtrer les Opération afin de ne montrer que celles qui ont eu lieu lors du mois en cours. Une vaiable filter de type boolean permet d'idiquer à la fonction refreshList(), qui recrée les éléments de la liste d'opération dans la page HTML à chaque fois qu'elle est modifiée, si elle doit filtrer ou non la liste lorsqu'elle s'execute.

## Etrangetés :

le Date.getMonth() retourne "3", alors que nous sommes les mois d'avrile donc le "4". J'ai donc pris sur moi d'ajouter "1 "au résulta afin de le fairee fonctionner. Le reste de la date est conrrecte...

Comme j'ai mis en place la fonctionnalité du filtre en fin de projet, il est bancale... il ne prend pas en compte l'année par exemple. Il aurait pu être perfectionné, mais ca aurait générer des problèmes en fin de projet donc... On va garder ces problématiques pour le futur projet Angular.












