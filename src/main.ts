import { Account } from './entities';
import { Operation } from "./entities";

// Account :
const accountName = document.querySelector<HTMLElement>('#accountName');
const accountSold = document.querySelector<HTMLElement>('#accountSolde');
const dateSold = document.querySelector<HTMLElement>('#dateSolde');
const numberOp = document.querySelector<HTMLElement>('#numberOp');
const perMonth = document.querySelector<HTMLElement>('#perMonth');
const all = document.querySelector<HTMLElement>('#all');

let filter = false;

// Jeux de données Account :
let Jean = new Account("Jean Louis", 2000);
let Michel = new Account("Michel Benmichel", 5676)
let Gérard = new Account("Gérard Depardieux", 16709845);
let accNum:number = Math.floor(Math.random() * 3);
let accountList: Account[] = [Jean, Michel, Gérard];
let base = accountList[accNum].sold;



let addOp = document.querySelector<HTMLElement>('#addOp');

accountName!.textContent = accountList[accNum].owner;
accountSold!.textContent = String(accountList[accNum].sold + " €");
dateSold!.textContent = generateDate();
numberOp!.textContent = String(0);

// Jeu de données Opérations :
let op0 = new Operation(100, "Ajout", generateDate(), "Papa", 0);
let op1 = new Operation(-245.56, "Dépense", generateDate(), "Vélo", 1);
let op2 = new Operation(-12, "Dépense", generateDate(), "Tabac", 2);
let opList: Operation[] = [op0, op1, op2];
opList.forEach(op => {
  createOp(op);
});
accountSold!.textContent = String(base + refreshTotal() + " €");
numberOp!.textContent = String(opList.length);

// Génération des opérations :
function listSelector(list : Operation[]) : Operation[] {
  let filteredList : Operation[] = []; 
  const date = new Date();
  let fixMonth = date.getMonth() + 1;
  let thisMonth = (date.getMonth() < 10) ? String("0" + fixMonth) : fixMonth; 
  if (filter) {
    filteredList = list.filter(op => op.date.split('-')[1] == thisMonth); 
  } else {
    filteredList = list;
  }
  return filteredList;
}

addOp?.addEventListener('click', (e) => {
  e.preventDefault();
  let newOp: Operation = takeInputData();
  opList.push(newOp);
  refreshList(listSelector(opList));

});

// Création des Opérations dans le DOM :
function createOp(newOp: Operation) {

  let section = document.querySelector<HTMLElement>('#sectionOp');

  let opFrame = document.createElement('div');
  opFrame.className = 'flex items-center flex-col justify-between mb-10 sm:flex-row hover:shadow-xl border p-8 rounded';

  let typeCol = document.createElement('div');
  typeCol.className = 'flex flex-col py-4 justify-around items-center';
  let typeColLabel = document.createElement('div');
  typeColLabel.className = 'mb-4 font-bold';
  typeColLabel.textContent = 'Type :';
  let typeColValue = document.createElement('div');
  typeColValue.textContent = newOp.type;

  let soldCol = document.createElement('div');
  soldCol.className = 'flex flex-col py-4 justify-around items-center';
  let soldColLabel = document.createElement('div');
  soldColLabel.className = 'mb-4 font-bold';
  soldColLabel.textContent = "Montant :";
  let soldColValue = document.createElement('div');
  soldColValue.textContent = String(newOp.amount);

  let dateCol = document.createElement('div');
  dateCol.className = 'flex flex-col py-4 justify-around items-center';
  let dateColLabel = document.createElement('div');
  dateColLabel.className = 'mb-4 font-bold';
  dateColLabel.textContent = "Date :";
  let dateColValue = document.createElement('div');
  dateColValue.textContent = newOp.date;

  let lastOpCol = document.createElement('div');
  lastOpCol.className = 'flex flex-col py-4 justify-around items-center';
  let lasOpColLabel = document.createElement('div');
  lasOpColLabel.className = 'mb-4 font-bold';
  lasOpColLabel.textContent = "Précision :";
  let lastOpColValue = document.createElement('div');
  lastOpColValue.textContent = newOp.details;

  let button = document.createElement('button');
  button.className = "hover:shadow-md"
  button.type = "button";
  button.textContent = "❌";

  typeCol.appendChild(typeColLabel);
  typeCol.appendChild(typeColValue);
  soldCol.appendChild(soldColLabel);
  soldCol.appendChild(soldColValue);
  dateCol.appendChild(dateColLabel);
  dateCol.appendChild(dateColValue);
  lastOpCol.appendChild(lasOpColLabel);
  lastOpCol.appendChild(lastOpColValue);
  opFrame.appendChild(typeCol);
  opFrame.appendChild(soldCol);
  opFrame.appendChild(dateCol);
  opFrame.appendChild(lastOpCol);
  opFrame.appendChild(button);
  section?.appendChild(opFrame);

  button.addEventListener('click', (e) => {
    e.preventDefault();
    deleteOp(newOp.index);
  });
}

function refreshTotal(): number {
  let tt: number = 0;
  opList.forEach(op => {
    tt += op.amount;
  });
  return tt;
}

// Récupération et traitemetndes Inputs :
function takeInputData(): Operation {

  const inputAmount = document.querySelector<HTMLInputElement>('#getAmount')!;
  const inputDate = document.querySelector<HTMLInputElement>('#getDate')!;
  const inputType = document.querySelector<HTMLInputElement>('#getType')!;
  const inputContext = document.querySelector<HTMLInputElement>('#getContext')!;
  const index: number = opList.length;

  let amount: number;

  if (inputAmount.value == "") {
    amount = 0;
    alert("Vous n'avez rentrez aucun montant... Nous vous avons créé une opération de montant 0")
  } else {
    amount = (inputType?.value == "Dépense") ?
    Number("-" + inputAmount?.value) :
    Number("+" + inputAmount?.value);
  }
  let date = (inputDate!.value == "") ? generateDate() : inputDate!.value;
  let type = inputType!.value;
  let context = (inputContext!.value == "") ? "aucun" : inputContext!.value;

  let newOp = new Operation(amount, type, date, context, index);

  inputAmount.value = "";
  inputDate.value = "";
  inputType.value = "Dépense";
  inputContext.value= "";

  return newOp;
}

// Fonction Delete :
function deleteOp(id: number) {
  opList = opList.filter(el => el.index != id);
  refreshList(listSelector(opList));
}

function refreshList(List : Operation[]) {
  accountSold!.textContent = String(base + refreshTotal() + " €");
  numberOp!.textContent = String(opList.length);
  let section = document.querySelector<HTMLElement>('#sectionOp');
  section!.innerHTML = "";
  for (const op of List) {
    createOp(op);
  }
}

// Générateur de date :
function generateDate(): string {
  const date = new Date();

  let day: string;
  let month: string;
  let year: string;

  year = String(date.getFullYear());
  if (date.getDate() < 10) {
    day = String("0" + date.getDate());
  } else {
    day = String(date.getDate());
  }
  let fixMonth = date.getMonth() + 1;
  if (date.getMonth() < 10) {
    month = String("0" + fixMonth);
  } else {
    month = String(date.getMonth());
  }
  let formatedDate: string = year + "-" + month + "-" + day;

  return formatedDate;
}

// Listener perMonth :
perMonth?.addEventListener('click', (e) => {
  e.preventDefault();
  filter = true;
  refreshList(listSelector(opList));
});

// Listener All :

all?.addEventListener('click', (e) => {
  e.preventDefault();
  filter = false;
  refreshList(listSelector(opList))
});







