export class Account {
  owner : string;
  sold : number;
  date : string = "";
  lastOp : string = "";

  constructor(owner:string, sold:number) {

    this.owner = owner;
    this.sold = sold;
  }
}

export class Operation {
  amount: number;
  type : string;
  date: string;
  details : string;
  index: number;

  constructor(amount:number, type:string, 
    date:string, details:string, index: number) {
    this.amount = amount;
    this.type = type;
    this.date = date;
    this.details = details;
    this.index = index;
  }

}